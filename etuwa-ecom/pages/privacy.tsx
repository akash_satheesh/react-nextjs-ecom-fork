import styles from '../styles/Home.module.css'

const Privacy = () => {
    return (
        <div className={styles.container}>
            <div>
                <h1>Privacy Policy</h1>
                <p>At Masskar, we understand that privacy is an important issue for visitors to the Masskar, Internet website and its related domains. The following information is designed to help visitors understand what information we collect from our site, and how we handle and use the information after that.</p>

                <h4>Information Collection and Use</h4>
                <p>Masskar, is the sole owner of any information collected on this site. We will not sell, share, or rent this information to others in ways different from what is disclosed in this statement. Masskar, may collect information from our users at several different points on our website.</p>

                <h4>General Enquiries </h4>
                <p>If a user contacts Masskar, with a general enquiry, the user may need to provide Masskar, with personal information (for example contact details). Masskar, will use this personal information to respond to the enquiry and track further correspondence. Personal information collected is not used for any other purpose.</p>

                <h4>Cookies</h4>
                <p>Cookies are small text files that a site transfers to a visitor’s hard disk or browser for added functionality or for tracking site usage. In order to measure the effectiveness of our online presence, Masskar may use cookies to determine the path users take on our site and to identify repeat visitors of our site. We do not use cookies to gather personal information such as a person’s name or e-mail address.</p>

                <h4>External Links </h4>
                <p>The Masskar website may contain links to other sites. Please take note that Masskar, is not responsible for the privacy policies or practices of such other sites. We encourage our users to be aware when they leave our site and to read the privacy statements of each and every site that collects personally identifiable information. This privacy statement applies solely to information collected by this site.</p>
                
                <h4>Changes to Masskar, Privacy Policy </h4>
                <p>Masskar, may, from time to time, make changes to this policy. We recommend that users of this site re-visit this privacy policy on occasion to learn of new privacy practices or changes to our policy.</p>

                <h4>Terms of Use</h4>
                <p>The information contained in the pages of our website is for information purposes only. Whilst every care has been taken in its preparation Masskar, its subsidiaries and associated companies do not make any warranties or representations as to its accuracy or reliability.</p>
                <p>In no event do we accept liability of any description including liability for negligence for any damages whatsoever resulting from loss of use, data or profits arising out of or in connection with the viewing, use or performance of this website or its contents.</p>
                <p>The pages contained in this Website may contain technical inaccuracies and typographical errors. The information in these pages may be updated from time to time and may at times be out of date.</p>
                <p>We accept no responsibility for keeping the information in these pages up to date or liability for any failure to do so.</p>
            </div>
        </div>
    );
}
export default Privacy;