import styles from '../styles/Home.module.css'

const Faq = () => {
    return (
            <div className={styles.container}>               
                <div>
                    <h1>FAQ'S</h1>
                    <h4>What does Masskar supply? Fresh or frozen seafood?</h4>
                    <p>‘Fresh seafood’ is a myth, unless it is cooked and eaten on the fishing boat itself. In fact, there’s no such thing as fresh seafood. We supply ‘As good as Live’ seafood through our world class cold chain network which travels through what is technically called the ‘Food Miles’ preserving all the freshness and nutrients.</p>
                    <h4>How are Masskar products processed? </h4>
                    <p>Packaging is very important. Contact with atmospheric air is responsible for decay of the fish. Wear and tear of the packaging causes spoilage. Masskar takes care of it all. What you get is secured in top-of-the-line double packaging.</p>
                
                    <h4>Will all this cost me more? </h4>
                    <p>Not at all. You pay at par with market prices. Moreover you will enjoy customer privileges and promos Daily Fish offers</p>

                    <h4>Any delivery charges? </h4>
                    <p>Yes, there is a Minimum Delivery Charge of 10 QAR per order irrespective of the number or cost of items you purchase from Masskar.</p>

                    <h4>What if I have to cancel my order? </h4>
                    <p>You can cancel it before the order is processed.</p>

                    <h4>Can I return my order? </h4>
                    <p>Certainly. If you are unhappy with product you can register a complaint by calling our helpline number or mailing us. Just make sure that packaging is not damaged when you take delivery, though there are very little chances of that happening.</p>

                    <h4>What is the payment methods? </h4>
                    <p>Cash on delivery Credit card swipe on delivery (Our delivery man will carry mobile credit card swiping machine)</p>
                </div>
            </div>
    );
}
export default Faq;