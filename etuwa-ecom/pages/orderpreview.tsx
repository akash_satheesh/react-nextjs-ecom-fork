/**
 Author: Sumith M S
 Component name: OrderPreview
 **/
import React from "react"
import * as cookie from 'cookie'
import styles from '../styles/orderpreview.module.css';
import { replaceBackSlash } from "../utilities/utilities";
import Image from 'next/image'
import { parseUserCookies } from "../helpers/parseCookie";

export async function getServerSideProps(context: any) {
    const parsedCookies = cookie.parse(context.req.headers.cookie);
    let userCookie: string = parsedCookies.user;
    let userCookieObj = JSON.parse(userCookie);
    const res = await fetch('https://qfreshonline.com/androidapp/json/cart?access_token=' + userCookieObj.access_token);
    const data = await res.json();
    return {
        props: { orderItems: data } // will be passed to the page component as props
    }
}


const orderPreview = ({ orderItems }: { orderItems: any }) => {
    return (
        <div className={styles.row}>
            <table className={styles.userCard}>
                <tr>
                    <td><b>Name</b></td>
                    <td><b>Deliver To</b></td>
                    <td><b>Order On</b></td>
                    <td><b>Mobile</b></td>
                    <td><b>Email</b></td>
                </tr>
                <tr>
                    <td>Test user</td>
                    <td className={styles.contentWidth}>test user test test tset usr</td>
                    <td>2021-09-16 13:23:21 </td>
                    <td>123456789</td>
                    <td>test@gmail.com</td>
                </tr>
            </table>
            <div className={`${styles.itemDetails} ${styles.itemCard}`}>
                {orderItems.items.map((orderItem: any) => <div>
                    <table>
                        <tr>
                            <td className={styles.imageWidth}><Image src={replaceBackSlash(orderItem.variant.main_image)} width={100} height={100} alt="item image" /></td>
                            <td className={styles.itemName}> <h4>{orderItem.variant.name}</h4></td>
                            <td> <h4>{orderItem.variant.price}</h4></td>
                            <td> <h4>{orderItem.variant.addon_name}</h4></td>
                            <td> <h4>{orderItem.variant.price_formatted}</h4></td>
                        </tr>
                    </table><hr />
                </div>
                )}
            </div>
            <div className={styles.deliveryCard}>
                <h4>DELIVERY INFORMATION</h4>
                <table>
                    <tr>
                        <td>No.of Items</td>
                        <td>1</td>
                    </tr>
                    <tr>
                        <td>Total</td>
                        <td>168</td>
                    </tr>
                    <hr className={styles.hr} />
                    <tr>
                        <td>Sub Total</td>
                        <td>168</td>
                    </tr>
                    <tr>
                        <td>Delivery Fee</td>
                        <td>Free</td>
                    </tr><hr className={styles.hr} />
                    <tr>
                        <td>Grand Total</td>
                        <td>168</td>
                    </tr>
                    <tr>
                        <td>Area</td>
                        <td>Select</td>
                    </tr><hr className={styles.hr} />
                    <tr>
                        <td>Delivery Time</td>
                        <td>11 : AM</td>
                    </tr>
                    <tr>
                        <td>Payment</td>
                        <td>COD</td>
                    </tr><hr className={styles.hr} />
                    <tr>
                        <td>Instruction</td>
                    </tr>
                    <tr>
                        <td colSpan={4}>tset testtset testtset testtset testtset test</td>
                    </tr>
                </table>

            </div>

        </div>

    );

}

export default orderPreview;