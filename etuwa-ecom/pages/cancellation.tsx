import styles from '../styles/Home.module.css'

const Cancellation = () => {
    return (
        <div className={styles.container}>
            <h1>Cancellation</h1>
            <p>Cancellation of your order by Masskar Customer: At any given time, Masskar will have full right to cancel your order by giving reason for the same. You as a customer can also cancel your order anytime up to the cut-off time which will vary for every delivery slot and will be mentioned in our website. If there is a payment already made, we will refund the money.</p>
            <p>For cancellations placed after the given cut off time, no payment is refundable to you. If we suspect any fraudulent transaction by any customer or any transaction which defies the terms & conditions of using the website, we at our sole discretion would cancel such orders. We will maintain a negative list of all fraudulent transactions, customers and would deny access to them or cancel any orders placed by them.</p>
        </div>
    );
}
export default Cancellation;