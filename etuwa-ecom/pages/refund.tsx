import styles from '../styles/Home.module.css'

const Refund = () => {
    return (
        <div className={styles.container}>
            <div className={styles.row}>
                <h1>Return and Refund</h1>
                  <ul>
                      <li>Return & Refunds we have a simple return policy which entitles all our customers to return the product at the time of delivery or within a period of 24 hours therefrom, if due to some reason they are not satisfied with the quality or freshness of the product or services from the Company.</li>
                      <li>For product quality related matters, we will take the returned product back and issue another set of the same product if available.</li>
                      <li>If the customer is not willing to accept the product again, we will cancel the order. Company reserves the right to decide upon the return policy and can withdraw/modify the policy at any given time without any prior notice.</li>
                      <li>Any misuse of the Return Policy will not be entertained and suitable action will be taken thereupon.</li>
                  </ul>
            </div>
        </div>
    );
}
export default Refund;