import Header from './header'
import Navbar from './navbar'
import Footer from './footer'

interface IProps {
    // any other props that come into the component, you don't have to explicitly define children.
}

const Layout: React.FC<IProps> = ({ children, ...props }) => {
    return (
        <div>
            <Header />
            <Navbar />
            <div>{children}</div>
            <Footer />
        </div>    
    );
}

export default Layout