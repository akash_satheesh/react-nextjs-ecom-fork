export const replaceBackSlash = (str: string): any => {
    if(!!str)
        str = str.replace(/\\/g, '');
    return str;
}